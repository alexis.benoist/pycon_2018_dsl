from pyparsing import Word, ZeroOrMore


def parse(expr, string):
    return expr.parseString(string, parseAll=True)[0]


def parse_mult(s, loc, tokens):
    result = 1
    for t in tokens[::2]:
        result *= t
    return result


def parse_plus(s, loc, tokens):
    return sum(tokens[::2])

def parse_int(s, loc, tokens):
    return int(tokens[0])


def compute(string):
    number = (Word('0123456789')).setParseAction(parse_int)
    term = (number + ZeroOrMore('*' + number)).setParseAction(parse_mult)
    expr = (term + ZeroOrMore('+' + term)).setParseAction(parse_plus)
    return parse(expr, string)
