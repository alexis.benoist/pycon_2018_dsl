import pytest

from main import compute

# But du talk:
"""Donner une overview des DSL et une implémentation"""
# C'est quoi un DSL?
"""
Ca veut dire Domain Specific Language.
C'est un langage spécifique à un domaine
Différent d'un language générique comme python
Exemple de dsl:
 * dot
 * Gherkin
 * markdown
"""

# Pourquoi on fait un DSL?
"""
Leur spécificité et manque de pouvoir génére de la:
 * simplicité
 * sécurité
=> Un DSL permet aux utilisateurs d'être automones
pour leurs besoins quotidiens avec leur vocabulaire.
"""
# Mon utilisation d'un DSL?
"""
Un projet de détection de fraude dans l'assurance 
auto a permit aux experts métiers d'écrire leurs 
régles sous la forme d'un DSL:

 ((LICENSE_TYPE = '임시면허') and (time_of_incident_at_night = 'N'))
  or (TP_HOSPITAL_AMOUNT > TP_HOSPITAL_TIME * 2000) 
 
On va implémenter une sous partie de ce langage.
Le + et le * dans les opérations arithmétiques.
"""


def test_should_parse_a_number_1():
    assert 1 == compute('1')


def test_should_parse_a_number_2():
    assert 2 == compute('2')


def test_should_parse_a_number_3():
    assert 3 == compute('3')


def test_should_parse_a_number_with_multiple_digits():
    assert 31 == compute('31')


def test_should_compute_a_plus():
    assert 2 == compute('1 + 1')


def test_should_compute_two_plus():
    assert 111 == compute('1 + 10 + 100')


def test_should_compute_a_multiply():
    assert 6 == compute('2 * 3')


def test_should_compute_two_multiply():
    assert 12 == compute('2 * 3 * 2')


def test_should_compute_a_multiply_and_a_plus():
    assert 14 == compute('2 + 3 * 4')



# Conclusion
"""
On a implémenté un langage!
On a vu les raison des faire un DSL: langage commun, simplicité et sécurité
pour intéragir avec le métier.
"""
# Perspectives
"""
On pourrait ajouter d'autres fontionnalités: / - ()
On pourrait changer l'architecture du projet par exemple faire un véritable AST:
['1', '+', ['1', '*', '3']]
"""
# Questions
